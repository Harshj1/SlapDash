from django.db import models

# Create your models here.
from django.forms import model_to_dict


class Product(models.Model):
    category = models.CharField(max_length=150)
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=300)
    description = models.TextField()
    img_path = models.CharField(max_length=300)
    # manufacturer = models.CharField(max_length=300,
    # blank=True)
    price = models.DecimalField(max_digits=6,
    decimal_places=2)
    original_price = models.DecimalField(max_digits=6, decimal_places=2, default=400.00)
    quantity = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def product_obj(name, quantity):
        product = Product.objects.get(name=name)
        product.quantity += quantity
        if product.price > product.original_price * 70 / 100:
            product.price -= product.original_price * quantity / 150
        product.save()
        return Product.objects.get(name=name)
        # return product.product_id

class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=300)
    email_id = models.CharField(max_length=300, unique = True)
    password = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def user_obj(id):
        return User.objects.get(user_id=id)

class Buy(models.Model):
    id = models.AutoField(primary_key = True)
    product_id = models.ForeignKey('Product',
                                   on_delete=models.CASCADE, )
    user_id = models.ForeignKey('User',
                                on_delete=models.CASCADE, )
    quantity = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=6,
                                decimal_places=2, default=400.00)

    def __str__(self):  # __unicode__ on Python 2
        return "%s ---> %s" % (self.user_id, self.product_id)

    def products(user_id):
        user = User.objects.get(user_id=user_id)
        buy = Buy.objects.filter(user_id=user)
        print(buy)
        products = []
        for pro in buy:
            item = Product.objects.get(name=pro.product_id)
            items = {}
            items['item_name'] = item
            items['quantity'] = pro.quantity
            items['price'] = pro.price
            items['mul_price'] = pro.price * pro.quantity
            # items['img_src'] = item.values('img_path')
            products.append(items)

        # print(products)
        return products
