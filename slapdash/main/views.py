from django.http import *
from django.shortcuts import render, render_to_response, redirect
from .models import *
import json


def index(request):
    mens = Product.objects.filter(category='mens')[:8]
    womens = Product.objects.filter(category='womens')[:8]
    bags = Product.objects.filter(category='bags')[:8]
    footware = Product.objects.filter(category='footware')[:8]
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'index.html',
                      {"user_id": user_id, "name": name, 'mens': mens, 'womens': womens, 'bags': bags,
                       'footware': footware})

    else:
        return render(request, 'index.html', {'mens': mens, 'womens': womens, 'bags': bags, 'footware': footware})


def about(request):
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'about.html', {"user_id": user_id, "name": name})

    else:
        return render(request, 'about.html', {})


def contact(request):
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'contact.html', {"user_id": user_id, "name": name})

    else:
        return render(request, 'contact.html', {})


def mens(request):
    products = Product.objects.filter(category="mens")
    item = None
    if request.method == 'POST':
        received_json_data = json.loads(request.body)
        item = received_json_data['item']
        print(item)
        items = {}
        for var in item:
            items['product_name'] = var['_data']['item_name']
            print(items)
        print(items)
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'mens.html', {"user_id": user_id, "name": name, "products": products, "items": item})
    else:
        return render(request, 'mens.html', {"products": products, "items": item})


def single(request, id):
    item = Product.objects.filter(product_id=id).first()
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'single.html', {"user_id": user_id, "name": name, "item": item})

    else:
        return render(request, 'single.html', {"item": item})


def womens(request):
    products = Product.objects.filter(category="womens")

    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return render(request, 'womens.html', {"user_id": user_id, "name": name, "products": products})

    else:
        return render(request, 'womens.html', {"products": products})


def signin(request):
    if request.method == "POST":
        username = request.POST['Email']
        p = User.objects.filter(email_id=username).first()
        if p != None:
            password = request.POST['password']
            if p.password == password:
                request.session['id'] = p.user_id
                request.session['name'] = p.name
            else:
                return HttpResponse("Wrong Password")
        else:
            return HttpResponse("Username not present")
    user_id = request.session['id']
    name = request.session['name']
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'), {"user_id": user_id, "name": name})


def signup(request):
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        print("Already logged in")
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'), {"user_id": user_id, "name": name})

    if request.method == "POST":
        username = request.POST['Email']
        p = User.objects.filter(email_id=username).first()
        if p is None:
            name = request.POST['Name']
            password = request.POST['password']
            con_pass = request.POST['confirm_password']
            if con_pass == password:
                key = User(email_id=username, name=name, password=password)
                key.save()
            else:
                return HttpResponse("Password Does Not Match")

        else:
            return HttpResponse("Username already exists")

    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'), {"user_id": user_id, "name": name})
    else:
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'), {})


def logout(request):
    try:
        del request.session['id']
    except:
        pass
    return redirect('/')


def checkout(request):
    item = None


    if request.method == 'POST':
        items = request.POST['items']
        # for var in items:
        #     print(var['_data']['item_name'])
        item = json.loads(items)
        itemss = []
        itemprice = []
        total_price = 0
        # print(item)
        for var in item:
            print(var['_data'])
            var['_data']['price'] = var['_total']
            total_price += var['_total']
            itemss.append(var['_data'])
            # itemprice.append(var['_total'])
        total_price = float("{0:.2f}".format(total_price))
        request.session['item'] = itemss

        if request.session.has_key('id'):
            user_id = request.session['id']
            name = request.session['name']
            return render(request, 'checkout.html', {'item': itemss, "user_id": user_id, "name": name, 'total_price': total_price})

        return render(request, 'checkout.html', {'item': itemss, 'total_price': total_price})


def out(request):
    if not request.session.has_key('id'):
        return HttpResponse("Please Login in")

    user_id = request.session['id']
    name = request.session['name']

    if request.method == 'GET':
        items = request.session['item']
        for item in items:
            print(item)
            product_quantity = item['quantity']
            product_obj = Product.product_obj(item['item_name'], product_quantity)
            user_obj = User.user_obj(user_id)
            # for quantity in range(product_quantity):
            buy = Buy(product_id=product_obj, user_id=user_obj, quantity=product_quantity, price=product_obj.price)
                # print("*****************************")
                # print(buy)
            buy.save()

    return redirect('/')
    # return render(request, 'index.html', {"user_id": user_id, "name": name})


def myorder(request):
    if request.session.has_key('id'):
        user_id = request.session['id']
        name = request.session['name']
        product = Buy.products(user_id)
        total_price = 0
        for var in product:
            total_price += var['mul_price']
        print(product)
        total_price = float("{0:.2f}".format(total_price))
        return render(request, 'myorder.html',
                      {"user_id": user_id, "name": name, 'item': product, 'total_price': total_price})
    else:
        return HttpRequest('Please Login to see the past orders')
