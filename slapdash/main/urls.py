from django.conf.urls import url, include
from . import views

urlpatterns = [
    # url(r'^admin/', admin.site.urls),
    url(r'^$', views.index, name='index'),
    url(r'^about/$', views.about, name='about'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^mens/$', views.mens, name='mens'),
    url(r'^mens/(?P<id>[0-9]+)/$', views.single, name='single'),
    url(r'^womens/(?P<id>[0-9]+)/$', views.single, name='single'),
    url(r'^womens/', views.womens, name='womens'),
    url(r'^signin/', views.signin, name='signin'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^logout/', views.logout, name='logout'),
    url(r'^checkout/', views.checkout, name='checkout'),
    url(r'^out/', views.out, name='out'),
    url(r'^myorders/', views.myorder, name='myorder'),
]
