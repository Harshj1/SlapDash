### SlapDash
## Synopsis
SlapDash is built on Python’s Django​ Web framework. SlapDash is an e-commerce site that incorporates the dynamic pricing aspect of the real-world stock market with a twist, in that the prices of the product fall as the demand rises.  Because the prices fall when demand rises, more the number of your people get onboard, the cheaper the commodity get for everyone!
## Screenshots
<h3>Logged In page</h3>
<br>
<p><img src="slapdash/Demo1.jpg"></p>
<h3>Products Section</h3>
<br>
<p><img src="slapdash/Demo2.jpg"></p>
<h3>Cart</h3>
<br>
<p><img src="slapdash/Demo3.jpg"></p>
<h3>Working</h3>
<p> As you can see, the price of the product has descreased as the number of purchases has increased. Between the first two purchases, the product has been purchased by many other customers which has led to its reduction in price. The same happens between the second and the third purchase.</p>
<br>
<p><img src="slapdash/Demo4.jpg"></p>